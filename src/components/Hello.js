import React, { Component } from 'react';

export default class Hello extends Component {
    render() {
        return(
            <div>
                <div className="container">
                    <div className="center landing">
                        <h1 className={"h1 primary"}>David Crandall</h1>
                        {/* <hr className="hr"/> */}
                        <h3>Full Stack Web Developer</h3>
                    </div>                
                </div>            
            </div>
        );
    }
};